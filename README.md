**Teste técnico desenvolvedor Front End**

Olá, tudo bem? Se você chegou até aqui é porque queremos conhecer mais suas habilidades e nos conectarmos! Pedimos que voce produza esse teste que foi proposto por nosso time de desenvolvimento OR para vaga de Front End. Nesse desafio, você vai resolver um problema real ;)

---

**Problema**

O gerente de produto sentiu a necessidade de criarmos uma página para ter um resumo breve sobre as movimentações no nosso meio de pagamento. Para nossa sorte, o time de design já criou um layout, agora você só precisa desenvolve-lo.

**Links**

Assets : https://drive.google.com/drive/folders/151lsYbiNfbvVe31XJaBU6ZO4wM9r4Y2j

Protótipo: https://xd.adobe.com/view/f72994c7-f182-49c2-8195-72c888bf4640-a27a/

---

**Instruções**

Você deverá criar um fork deste projeto, e desenvolver em cima do seu fork. Sinta-se a vontade para desenvolver como quiser.

O prazo para desenvolver este teste é de 2 dias.

---

**O que será avaliado**

- A maneira como o candidato utilizou o GIT;
- A qualidade do código;
- Responsividade.

